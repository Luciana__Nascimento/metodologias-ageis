# Metodologias ágeis

# Scrum – Cerimônias e Papéis

## Existem 2 tipos de processos:

* **Definidos:** São determinados o que deverá ser feito, quando e como. Quem já trabalhou com o “início e fim do projeto” sabe que a utilização de um processo definido não garante o sucesso.

* **Empíricos:** São aqueles que não se conhecem todas as variáveis de entrada para que possa estabelecer um processo repetível.

Quando dirigimos um carro do ponto A ao ponto B, o melhor caminho seria uma reta, mas durante o trajeto temos diversas correções que devemos fazer durante o percurso.

Processos empíricos são baseados em inspeção e adaptação devem ser utilizados sempre que os processos definidos não forem adequados devido a complexidade do projeto.

É como um software, não adianta levantar todo o sistema no começo, ele muda, o que hoje é prioridade, amanhã já não é mais, outro problema é o tempo, com escopo fechado não dá para mudar requisitos, projeto atrasa, gera uma enorme burocracia de “Changes” e isso nos dá uma grande dor de cabeça.

O Scrum vem como uma forma de melhor atender todos esses problemas, por exemplo, se um projeto tem escopo fechado, o Scrum durante suas Sprints vai mostrar que aquele prazo provavelmente não será viável e com o tempo a empresa começa a mudar essa ideia de datas e sim começar a ver a roda girando a cada entrega de Sprint.

## E então, o que é esse tal SCRUM?

É um processo de desenvolvimento iterativo e incremental que pode ser aplicado a qualquer produto ou no gerenciamento de qualquer atividade complexa, ou seja, podemos aplicar esse processo em qualquer tipo de projeto, até mesmo dentro das nossas vidas no nosso dia a dia e nas nossas tarefas, não apenas no desenvolvimento de software.

Foi criado por Jeff Sutherland e Ken Schwaber na década de 90, então não é algo novo, aqui no Brasil que demoramos demais para adotar esta metodologia.


## Os Papéis do SCRUM

O Scrum possui apenas 3, isso mesmo, APENAS 3 papéis:

* Product Owner
* Scrum Master
* Scrum Team

Detalhando melhor:


### Product Owner (P.O.)

* Responsável por garantir o Retorno sobre o Investimento (ROI);
* Conhece as necessidades do que precisa ser feito;
* Define os itens do Product Backlog;
* Prioriza os itens na Sprint Planning Meeting.


### Scrum Master (SM)

* Garante o uso da Metodologia
* É um líder e um facilitador
* Precisa tirar da frente qualquer impedimento


### Scrum Team (ST)

* Função do SCRUM Team é ser auto-gerenciado
* Define as metas dos Sprints
* Produz com qualidade e valor para o P.O.



## Cerimônias do Scrum

![ciclo-scrum](/uploads/673f88c1d946799ebb5682b830e504b2/ciclo-scrum.png)



**As cerimônias são:**

* Product Backlog
* Sprint Planning Meeting
* Daily Scrum
* Sprint Review Meeting
* Sprint Retrospective


## Product Backlog

Lista com as funcionalidades para o produto, onde:

* O conteúdo é definido pelo P. O.
* Não necessita estar completo, ou seja, você não vai levantar todo o sistema de uma vez, com o tempo o Product Backlog cresce ou diminui dependendo do que o P.O. realmente necessita.


## Sprint Planning Meeting

Reunião com o P.O., Scrum Master e Scrum Team, onde:

* P.O. descreve as funcionalidades;
* A equipe questiona;
* No final é gerado o Sprint Backlog;
* Scrum Team e o P.O. definirão o **objetivo**;
* Nesta fase que temos o **Planning Poker** para definir o esforço da Sprint.

#### Planning Poker

![Planning_poker_deck_Serpro](/uploads/6807295bfec413db9569fa44a4b6a20a/Planning_poker_deck_Serpro.jpg)

São Cards que definirão pontos de esforço para desenvolver determinada tarefa passada e assim, dependendo o número de pontos, quais entrarão ou não no Sprint Backlog.

Estimar o **esforço das funcionalidades;**
Números **menores** mais **simples**;
Números **maiores** são mais **complexos**.

**Cartas Coringas:**

**Café** – 15 minutos para uma pausa no qual será discutida aquela funcionalidade
**?** – Não foi entendido alguma funcionalidade falada, o P.O. deverá explicá-la novamente.

**OBS:** Se em um time de 6 pessoas, uma votar por exemplo (1), outra (13) e os demais votarem (5). Cabe ao Scrum Master perguntar à aquele que votou no menor valor o motivo de sua escolha e em seguida também ao que votou no maior valor, e em seguida jogar novamente para ver se a atividade realmente é a de valor maior, menor ou média.

Isto é feito para que nenhuma dúvida passe, pode ser que alguém não entendeu e a tarefa esteja subestimada ou até mesmo superestimada num time de 6 pessoas, 1 votar por exemplo 1 e outra 13, os demais votarem 5, deverá o Scrum Master perguntar aquele que votou o menor valor o porque que foi votado aquele valor, deverá perguntar para aquele que votou o maior valor, o porque ele votou aquele valor e em seguida jogar novamente para ver se a atividade realmente é a de maior valor, menor ou a média, isto é feito para que nenhuma dúvida passe, pode ser que alguém não entendeu, a tarefa esteja subestimada ou superestimada.

**OBS 2:** Scrum Master e P.O. **NÃO** jogam e **NÃO** opinam sobre os pontos.

Pontos e tempo:

![pontostempo](/uploads/96ecb54ad0e876adc85b26381c8f2a25/pontostempo.png)

Finalmente definido o Sprint Backlog damos início para a Sprint.


## Sprint


* Funcionalidades escolhidas na Sprint Planning Meeting ficarão na coluna À Fazer (To Do);
* É definido um prazo para o Sprint (2 ou 4 semanas);
* O prazo é mantido até o final do projeto, **não pode mudar**, ora Sprint de uma semana, ora de quatro, **isso NÃO existe;**
* Considera-se finalizado com Sucesso: Quando **todas as tarefas estiverem na Coluna Finalizado (DONE);**
* Finalizado com **falha:** Quando o **tempo estourar** (2 semanas por exemplo) e/ou funcionalidades **mal implementadas** e **sem qualidade.**


#### Kanban

No quadro é colocado o andamento do Sprint

* Os Post its são importantes, se coloridos facilitam a visualização
* Por ele acompanhamos o que está sendo desenvolvido

#### Post It

Os post its coloridos vão facilitar para ver o que deverá ser atacado primeiro:

* Azul: tarefas simples, esforço 1 ou 2
* Amarelo: tarefas de médio esforço (3 ou 5)
* Vermelho: tarefas complexas, esforço de 8 ou mais

Se há itens em vermelho ou amarelo a prioridade é **eliminá-los logo no começo da Sprint** para garantir a entrega e não deixar para queimar a “gordura” no final da Sprint, o que provavelmente vai ocasionar uma falha.


## Daily Scrum

Reunião em pé de 15 minutos no máximo feita todos os dias (mesmo quando o SM não estiver presente).

São respondidas 3 perguntas:

1. O que você fez ontem?
2. O que você fará hoje?
3. Há algum impedimento no seu caminho?

A Daily não é para resolver problemas, não é um status report.


## Sprint Review Meeting

No final de cada Sprint Retrospective é feito um Sprint Review Meeting, onde:

* É mostrado o que foi alcançado no Sprint;
* Nesta reunião estará o P.O., Scrum Team e Scrum Master;
* O mais **importante** é que o **objetivo** esteja **realizado**.


## Sprint Retrospective

Também ocorre ao final da Sprint, onde o time identifica:

* O que funcionou bem;
* O que poderia ser melhorado;
* Quais ações serão tomadas para melhoria;



## E depois?

Pois bem, este é o Scrum, não há segredos, desde que usado corretamente, nunca podemos remover cerimônias, nunca podemos deixar de fazer as dailys e não há porque dar errado.

Em outras palavras o SCRUM FUNCIONA SIM, quando mal interpretado ou mal utilizado não funcionará como qualquer outra metodologia.

Utilizando bem Scrum podemos agregar boas práticas de outras metodologias, por exemplo, Programação em par, dentre outras, pois com Scrum não podemos remover itens, mas podemos agregar.









